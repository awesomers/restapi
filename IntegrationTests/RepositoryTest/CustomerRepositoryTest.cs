﻿using Castle.Core;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using DataAccessLayer.Models;
using DataAccessLayer.Repositories;
using NUnit.Framework;
using Neo4jClient;
using StockRestApi.Factory;

namespace IntegrationTests.RepositoryTest
{
    public class CustomerRepositoryTest
    {
        [Test,Ignore]
        public void ShouldAddStockInfo()
        {
            var windsorContainer = new WindsorContainer();
            windsorContainer.Register(
                Component.For<IGraphClient>().LifeStyle.Is(LifestyleType.Singleton).UsingFactoryMethod(
                    GraphClientFactory.CreateNewGraphClient
                    ),
                Component.For<IShareRepository>().ImplementedBy<ShareRepository>()
                );

            CustomerRepository customerRepository = new CustomerRepository(windsorContainer.Resolve<IGraphClient>(), windsorContainer.Resolve<IShareRepository>());


            Node<Customer> user = customerRepository.GetUser("p@p.com");
            Node<Customer> userFromSession = customerRepository.GetUserFromSession("a");


        }


    }
}
