using DataAccessLayer.Models;
using Neo4jClient;

namespace DataAccessLayer.Relationships
{
    public class CustomerOwnsShare :
        Relationship,
        IRelationshipAllowingSourceNode<Customer>,
        IRelationshipAllowingTargetNode<DALStock>
    {
        public CustomerOwnsShare(NodeReference targetNode)
            : base(targetNode)
        {
            
        }
        
//        public int Quantity { get; set; }
//        public float CostPerShare { get; set; }
//        public string DateOfPurchase { get; set; }
//        public string ShareSymbol { get; set; }

        public const string TypeKey = "CUSTOMER_OWNS_SHARE";
        public override string RelationshipTypeKey
        {
            get { return TypeKey; }
        }
    }
}