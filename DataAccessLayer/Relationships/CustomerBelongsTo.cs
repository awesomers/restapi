﻿using DataAccessLayer.Models;
using Neo4jClient;

namespace DataAccessLayer.Relationships
{
    public class CustomerBelongsTo :
            Relationship,
            IRelationshipAllowingSourceNode<Customer>,
            IRelationshipAllowingTargetNode<RootNode>
    {
        public CustomerBelongsTo(NodeReference targetNode)
            : base(targetNode)
        {
        }

        public const string TypeKey = "CUSTOMER_BELONGS_TO";
        public override string RelationshipTypeKey
        {
            get { return TypeKey; }
        }
    }
}
