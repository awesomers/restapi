using DataAccessLayer.Models;
using Neo4jClient;

namespace DataAccessLayer.Relationships
{
    public class ShareBelongsTo :
        Relationship,
        IRelationshipAllowingSourceNode<DALShare>,
        IRelationshipAllowingTargetNode<RootNode>
    {
        public ShareBelongsTo(NodeReference targetNode)
            : base(targetNode)
        {
        }

        public const string TypeKey = "STOCK_BELONGS_TO";
        public override string RelationshipTypeKey
        {
            get { return TypeKey; }
        }
    }
}