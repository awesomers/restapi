using DataAccessLayer.Models;
using Neo4jClient;

namespace DataAccessLayer.Relationships
{
    public class ShowsOffTo :
        Relationship,
        IRelationshipAllowingSourceNode<Customer>,
        IRelationshipAllowingTargetNode<Customer>
    {
        public ShowsOffTo(NodeReference targetNode)
            : base(targetNode)
        {
            
        }
        
        public const string TypeKey = "SHOWS_OFF_TO";
        public override string RelationshipTypeKey
        {
            get { return TypeKey; }
        }
    }
}