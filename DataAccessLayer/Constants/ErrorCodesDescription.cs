﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Constants
{
    public class ErrorCodesDescription
    {
       internal static string E001
        {
            get { return "EmailAddress is already in use."; }
        }

        public static string E002
        {
            get { return "Stock Already Created"; }
        }

        public static string E003
        {
            get { return "Could not add Stock Info"; }
        }
        
        public static string E005
        {
            get { return "Could not Show Off to Freind"; }
        }
    }
}
