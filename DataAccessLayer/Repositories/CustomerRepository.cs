﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DataAccessLayer.Constants;
using DataAccessLayer.Models;
using DataAccessLayer.Relationships;
using Neo4jClient;
using Neo4jClient.Gremlin;

namespace DataAccessLayer.Repositories
{
    public interface ICustomerRepository
    {
        IEnumerable<Node<Customer>> GetUsers();
        PersistenceResult<NodeReference> CreateUser(Customer customer);

        IEnumerable<Node<DALStock>> GetStocks(string email, int pageNumber);
        IEnumerable<Node<DALStock>> GetStocks(string email);
        PersistenceResult<NodeReference> AddStockInfo(string email, DALStock stock);

        Node<Customer> GetUser(string email);
        Node<Customer> GetUserFromSession(string sessionId);
        IEnumerable<DALFriend> SearchUser(string friend);

        PersistenceResult<NodeReference> UpdateUser(string oldEmail, Customer proposedCustomer);
        void DeleteUserByUserNode(NodeReference<Customer> userNode);
        PersistenceResult<RelationshipReference> ShowOff(string email, string targetFriendEmail);

        IEnumerable<DALFriend> GetFollowers(string sessionIdInBytes);
        IEnumerable<DALFriend> GetFollowing(string sessionIdInBytes);
    }

    public class CustomerRepository : ICustomerRepository
    {
        private readonly IGraphClient graphClient;
        private readonly IShareRepository shareRepository;

        public CustomerRepository(IGraphClient graphClient, IShareRepository shareRepository)
        {
            this.graphClient = graphClient;
            this.shareRepository = shareRepository;
        }

        #region ICustomerRepository Members

        public PersistenceResult<NodeReference> CreateUser(Customer customer)
        {
            var modelState = new ModelStateDictionary();

            customer.Email = customer.Email.ToLowerInvariant();

            int nodeCount =
                graphClient.RootNode.In<Customer>(CustomerBelongsTo.TypeKey, c => c.Email == customer.Email).
                    GremlinCount();

            if (nodeCount > 0)
                modelState.AddModelError("E001", ErrorCodesDescription.E001);

            if (!modelState.IsValid)
                return PersistenceResult<NodeReference>.Failure(modelState, null);

            RootNode rootNode = graphClient.RootNode;

            NodeReference<Customer> node = graphClient.Create(
                customer,
                new CustomerBelongsTo(rootNode));

            return PersistenceResult<NodeReference>.Success(node);
        }
        public Node<Customer> GetUser(string email)
        {
            IGremlinNodeQuery<Customer> gremlinNodeQuery = graphClient
                .RootNode
                .In<Customer>(CustomerBelongsTo.TypeKey, c => c.Email == email);

            return gremlinNodeQuery
                .SingleOrDefault();
        }
        public Node<Customer> GetUserFromSession(string sessionId)
        {
            IGremlinNodeQuery<Customer> gremlinNodeQuery = graphClient
                .RootNode
                .In<Customer>(CustomerBelongsTo.TypeKey, c => c.SessionId == sessionId);

            return gremlinNodeQuery
                .SingleOrDefault();
        }

        public PersistenceResult<NodeReference> AddStockInfo(string email, DALStock stock)
        {
            var modelState = new ModelStateDictionary();

            Node<Customer> user = GetUser(email);

            NodeReference<DALStock> nodeReference = graphClient.Create(stock, new CustomerOwnsShare(user.Reference));
            //TODO MUST, I think I've screwed up direction here, cross check !

            if (nodeReference != null)
            {
                return PersistenceResult<NodeReference>.Success(nodeReference);
            }
            modelState.AddModelError("E003", ErrorCodesDescription.E003);
            return PersistenceResult<NodeReference>.Failure(modelState, null);
        }

        public IEnumerable<Node<DALStock>> GetStocks(string email, int pageNumber)
        {
            IEnumerable<Node<DALStock>> stocks =
                graphClient.RootNode.In<Customer>(CustomerBelongsTo.TypeKey, c => c.Email == email).Out<DALStock>(
                    CustomerOwnsShare.TypeKey).Skip(MiscellaneousConstants.PageSize*(pageNumber - 1)).Take(
                        MiscellaneousConstants.PageSize);

            return stocks;
        }
        
        public IEnumerable<Node<DALStock>> GetStocks(string email)
        {
            IEnumerable<Node<DALStock>> stocks =
                graphClient.RootNode.In<Customer>(CustomerBelongsTo.TypeKey, c => c.Email == email).Out<DALStock>(
                    CustomerOwnsShare.TypeKey);
            return stocks;
        }


        

       

        public IEnumerable<DALFriend> SearchUser(string friend)
        {
            IEnumerable<DALFriend> friends =
                graphClient.RootNode.In<Customer>(CustomerBelongsTo.TypeKey,
                                                  c => c.Name == friend)
                                                  .Select(
                                                      node => new DALFriend
                                                                  {
                                                                      Email = node.Data.Email,
                                                                      Name = node.Data.Name
                                                                  });
            return friends;
        }

        public PersistenceResult<NodeReference> UpdateUser(string oldEmail, Customer proposedCustomer)
        {
            var modelState = new ModelStateDictionary();

            Node<Customer> oldCustomerNode = GetUser(oldEmail.ToLowerInvariant());
            if (oldCustomerNode == null)
            {
                modelState.AddModelError("Email", @"The customer Email does not exist.");
                return PersistenceResult<NodeReference>.Failure(modelState, null);
            }

            if (oldEmail != proposedCustomer.Email && GetUser(proposedCustomer.Email) != null)
            {
                modelState.AddModelError("Email", @"Email Address is already in use.");
            }

            if (!modelState.IsValid)
                return PersistenceResult<NodeReference>.Failure(modelState, null);

            graphClient.Update(oldCustomerNode.Reference, c =>
                                                              {
                                                                  c.Email = proposedCustomer.Email.ToLowerInvariant();
                                                                  c.Name = proposedCustomer.Name;
                                                                  c.Password = proposedCustomer.Password;
                                                              });

            return PersistenceResult<NodeReference>.Success(oldCustomerNode.Reference);
        }

        public void DeleteUserByUserNode(NodeReference<Customer> userNode)
        {
            graphClient.Delete(userNode, DeleteMode.NodeAndRelationships);
        }

        public PersistenceResult<RelationshipReference> ShowOff(string customerEmail, string targetFriendEmail)
        {
            var modelState = new ModelStateDictionary();

            Node<Customer> targetFriend = GetUser(targetFriendEmail);
            Node<Customer> customer = GetUser(customerEmail);
            RelationshipReference relationshipReference = graphClient
                .CreateRelationship(customer.Reference, new ShowsOffTo(targetFriend.Reference));

            if (relationshipReference == null)
            {
                modelState.AddModelError("E005", ErrorCodesDescription.E005);
                return PersistenceResult<RelationshipReference>.Failure(modelState,null);
            }
            return PersistenceResult<RelationshipReference>.Success(relationshipReference);

        }

        public IEnumerable<DALFriend> GetFollowers(string sessionIdInBytes)
        {
            Node<Customer> user = GetUserFromSession(sessionIdInBytes);
            return graphClient.RootNode
                .In<Customer>(CustomerBelongsTo.TypeKey, c => c.Email == user.Data.Email)
                .Out<Customer>(ShowsOffTo.TypeKey)
                .Select(node =>
                            {
                                return new DALFriend
                                           {
                                               Email = node.Data.Email,
                                               Name = node.Data.Name
                                           };
                            });
        }

        public IEnumerable<DALFriend> GetFollowing(string sessionIdInBytes)
        {
            Node<Customer> user = GetUser(sessionIdInBytes);
            return graphClient.RootNode
                .In<Customer>(CustomerBelongsTo.TypeKey, c => c.Email == user.Data.Email)
                .In<Customer>(ShowsOffTo.TypeKey)
                .Select(node =>
                            {
                                return new DALFriend
                                           {
                                               Email = node.Data.Email,
                                               Name = node.Data.Name
                                           };
                            });
        }

        public IEnumerable<Node<Customer>> GetUsers()
        {
            //don't use this FATAL !!! if user count grows, highly imperformant, use the paginated one.
            return graphClient.RootNode
                .In<Customer>(CustomerBelongsTo.TypeKey)
                .OrderBy(c => c.Data.Email)
                .ToList();
        }

        #endregion
    }
}