﻿using System.Linq;
using System.Web.Mvc;
using DataAccessLayer.Constants;
using DataAccessLayer.Models;
using DataAccessLayer.Relationships;
using Neo4jClient;
using Neo4jClient.Gremlin;

namespace DataAccessLayer.Repositories
{
    public class ShareRepository : IShareRepository
    {
        private readonly IGraphClient graphClient;

        public ShareRepository(IGraphClient graphClient)
        {
            this.graphClient = graphClient;
        }

        public PersistenceResult<NodeReference> CreateShare(DALShare share)
        {
            var modelState = new ModelStateDictionary();

            int nodeCount =
                graphClient.RootNode.In<DALShare>(ShareBelongsTo.TypeKey, s => s.Symbol == share.Symbol).
                    GremlinCount();

            if (nodeCount > 0)
                modelState.AddModelError("E002", ErrorCodesDescription.E002);

            if (!modelState.IsValid)
                return PersistenceResult<NodeReference>.Failure(modelState, null);

            RootNode rootNode = graphClient.RootNode;

            NodeReference<DALShare> node = graphClient.Create(
                share,
                new ShareBelongsTo(rootNode));

            return PersistenceResult<NodeReference>.Success(node);
        }

        public Node<DALShare> GetShare(string symbol)
        {
            return graphClient
                .RootNode
                .In<DALShare>(ShareBelongsTo.TypeKey, s => s.Symbol== symbol)
                .SingleOrDefault();

        }
    }

    public interface IShareRepository
    {
        PersistenceResult<NodeReference> CreateShare(DALShare share);
        Node<DALShare> GetShare(string symbol);
    }
}
