﻿using System.Web.Mvc;

namespace DataAccessLayer.Repositories
{
    public class PersistenceResult<TData> : PersistenceResult
    {
        readonly TData data;

        protected PersistenceResult(ModelStateDictionary modelState, TData data)
            : base(modelState)
        {
            this.data = data;
        }

        public TData Data
        {
            get { return data; }
        }

        public static PersistenceResult<TData> Success(TData data)
        {
            return new PersistenceResult<TData>(new ModelStateDictionary(), data);
        }

        public static PersistenceResult<TData> Failure(ModelStateDictionary state, TData data)
        {
            return new PersistenceResult<TData>(state, data);
        }
    }

}