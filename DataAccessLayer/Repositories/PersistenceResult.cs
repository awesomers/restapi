﻿using System.Web.Mvc;

namespace DataAccessLayer.Repositories
{
    public class PersistenceResult
    {
        readonly ModelStateDictionary modelState;

        protected PersistenceResult(ModelStateDictionary modelState)
        {
            this.modelState = modelState;
        }

        public static PersistenceResult Success()
        {
            return new PersistenceResult(new ModelStateDictionary());
        }

        public static PersistenceResult Failure(ModelStateDictionary state)
        {
            return new PersistenceResult(state);
        }

        public ModelStateDictionary ModelState
        {
            get { return modelState; }
        }

        public bool IsValid
        {
            get { return modelState.IsValid; }
        }

    }

}