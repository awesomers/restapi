﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Models
{
    public class DALStock
    {
        public string ShareSymbol { get; set; }
        public int Quantity { get; set; }
        public float CostPerShare { get; set; }
        public string DateOfPurchase { get; set; }
    }
}
