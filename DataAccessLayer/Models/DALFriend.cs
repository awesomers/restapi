namespace DataAccessLayer.Models
{
    public class DALFriend
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}