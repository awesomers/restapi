﻿namespace ShareService.Models
{
    public class CurrentShareInfo
    {
        public string Symbol;
        public string Company;
        public string Exchange ;
        public string Currency ;
        public double LatestPrice;
    }
}
