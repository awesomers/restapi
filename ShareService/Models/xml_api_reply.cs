namespace ShareService.Models
{
    public class xml_api_reply
    {
        public finance finance;

    }

    public class finance
    {
        public string symbol;
        public string pretty_symbol;
        public string symbol_lookup_url;
        public string company;
        public string exchange;
        public string exchange_timezone;
        public string exchange_utc_offset;
        public string exchange_closing;
        public string divisor;
        public string currency;
        public string last;
        public string high;
        public string low;
        public string volume;
        public string avg_volume;
        public string market_cap;
        public string open;
        public string y_close;
        public string change;
        public string perc_change;
        public string delay;
        public string trade_timestamp;
        public string trade_date_utc;
        public string trade_time_utc;
        public string current_date_utc;
        public string current_time_utc;
        public string symbol_url;
        public string chart_url;
        public string disclaimer_url;
        public string ecn_url;
        public string isld_last;
        public string isld_trade_date_utc;
        public string isld_trade_time_utc;
        public string brut_last;
        public string brut_trade_date_utc;
        public string brut_trade_time_utc;
        public string daylight_savings;

    }
}