using ShareService.Models;

namespace ShareService.Services
{
    public interface IShareService
    {
        CurrentShareInfo GetShareInfo(string symbol);
    }
}