﻿using System;
using System.IO;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using ShareService.Models;

namespace ShareService.Services
{
    public class GoogleShareService : IShareService
    {

        public CurrentShareInfo GetShareInfo(string symbol)
        {
            var currentShareInfo = new CurrentShareInfo();
            if (!string.IsNullOrEmpty(symbol))
            {
                using (XmlReader reader = XmlReader.Create("http://www.google.com/ig/api?stock=" + symbol))
                {
                    reader.ReadToFollowing("symbol");
                    reader.MoveToAttribute("data");
                    currentShareInfo.Symbol = reader.Value;

                    reader.ReadToFollowing("company");
                    reader.MoveToAttribute("data");
                    currentShareInfo.Company = reader.Value;


                    reader.ReadToFollowing("currency");
                    reader.MoveToAttribute("data");
                    currentShareInfo.Currency = reader.Value;

                    reader.ReadToFollowing("last");
                    reader.MoveToAttribute("data");
                    try
                    {
                        currentShareInfo.LatestPrice = Convert.ToDouble(reader.Value);
                    }
                    catch (Exception)
                    {
                        currentShareInfo.LatestPrice = 0;
                    }
                }
            }
            return currentShareInfo;
        }


        public xml_api_reply GetCompleteShareInfo(string symbol)
        {
            var webRequest = WebRequest.Create("http://www.google.com/ig/api?stock=" + symbol) as HttpWebRequest;
            xml_api_reply completeShareInfo;

            using (var webResponse = webRequest.GetResponse() as HttpWebResponse)
            {
                HttpStatusCode httpStatusCode = webResponse.StatusCode;
                Stream responseStream = webResponse.GetResponseStream();
                var streamReader = new StreamReader(responseStream);

                string responseString = streamReader.ReadToEnd();

                var xmlSerializer = new XmlSerializer(typeof (xml_api_reply));
                completeShareInfo = (xml_api_reply) xmlSerializer.Deserialize(new StringReader(responseString));
                //needs fixing doesn't read attributes, lol

            }
            return completeShareInfo;
        }
    }
}