﻿using System.Text;

namespace Logic.Utils
{
    public static class CommonUtils
    {
        public static byte[] GenerateSuccessSessionToken()
        {
            var successSessionToken = new byte[16];

            System.Security.Cryptography.RandomNumberGenerator.Create().GetBytes(successSessionToken);
            return successSessionToken;
        }

        public static byte[] CreateHashedPassword(string password)
        {
            var salt = Encoding.Unicode.GetBytes("TATA SALT; BHARATH KA SALT");
            //            System.Security.Cryptography.RandomNumberGenerator.Create().GetBytes(salt);

            byte[] plainTextBytes = Encoding.Unicode.GetBytes(password);

            // Append salt to pwd before hashing
            var combinedBytes = new byte[plainTextBytes.Length + salt.Length];
            System.Buffer.BlockCopy(plainTextBytes, 0, combinedBytes, 0, plainTextBytes.Length);
            System.Buffer.BlockCopy(salt, 0, combinedBytes, plainTextBytes.Length, salt.Length);

            // Create hash for the pwd+salt 
            System.Security.Cryptography.HashAlgorithm hashAlgo = new System.Security.Cryptography.SHA256Managed();
            byte[] hash = hashAlgo.ComputeHash(combinedBytes);

            // Append the salt to the hash
            var hashPlusSalt = new byte[hash.Length + salt.Length];
            System.Buffer.BlockCopy(hash, 0, hashPlusSalt, 0, hash.Length);
            System.Buffer.BlockCopy(salt, 0, hashPlusSalt, hash.Length, salt.Length);

            return hashPlusSalt;
        }

    }
}
