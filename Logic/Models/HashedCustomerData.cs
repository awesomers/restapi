﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logic.Models
{
 public   class HashedCustomerData
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public byte[] Password { get; set; }


    }
}
