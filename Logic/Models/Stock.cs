﻿using System;

namespace Logic.Models
{
    public class Stock
    {
        public string ShareSymbol { get; set;  }
        public int Quantity { get; set; }
        public float CostPerShare { get; set; }
        public string DateOfPurchase { get; set; }
        public double LatestPrice { get; set; }
    }
}
