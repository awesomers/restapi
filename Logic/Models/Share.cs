﻿namespace Logic.Models
{
    public class Share
    {
        public string Symbol;
        public string Company;
        public string Exchange = "NASDAQ";
        public string Currency = "USD";

    }
}
