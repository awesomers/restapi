﻿using System.Collections.Generic;

namespace Logic.Models
{
    public class Profile : StockoResponse
    {
        public string Name { get; set; }
        public List<Stock> Stocks { get; set; }
    }
}
