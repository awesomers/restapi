namespace Logic.Models
{
    public class Friend : StockoResponse
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public double Improvement { get; set; }
    }
}