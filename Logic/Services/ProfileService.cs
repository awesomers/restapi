﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using DataAccessLayer.Models;
using DataAccessLayer.Repositories;
using Logic.Models;
using MiscUtil.Reflection;
using Neo4jClient;
using ShareService.Models;
using ShareService.Services;
using System.Linq;

namespace Logic.Services
{
    public class ProfileService : IProfileService
    {
        private readonly ICustomerRepository customerRepository;
        private readonly IShareRepository shareRepository;
        private readonly IShareService shareService;

        public ProfileService(ICustomerRepository customerRepository, IShareRepository shareRepository,
                              IShareService shareService)
        {
            this.customerRepository = customerRepository;
            this.shareRepository = shareRepository;
            this.shareService = shareService;
        }

        #region IProfileService Members

        public Profile GetProfileData(string sessionId, int pageNumber)
        {
            byte[] sessionIdInBytes = Encoding.Unicode.GetBytes(sessionId);

            Node<Customer> customer = customerRepository.GetUserFromSession(sessionId);
            //TODO chk null 
            if (customer==null)
            {
                //TODO some logging???
                return null;
            }
            IEnumerable<Node<DALStock>> stocks = customerRepository.GetStocks(customer.Data.Email, pageNumber);
            //TODO chk for null stocks
            var profile = new Profile
                              {
                                  Name = customer.Data.Name,
                                  Stocks = new List<Stock>()
                              };

            foreach (var stock in stocks)
            {
                double latestPrice = shareService.GetShareInfo(stock.Data.ShareSymbol).LatestPrice;
                profile.Stocks.Add(new Stock
                                       {
                                           CostPerShare = stock.Data.CostPerShare,
                                           DateOfPurchase = stock.Data.DateOfPurchase,
                                           Quantity = stock.Data.Quantity,
                                           ShareSymbol = stock.Data.ShareSymbol,
                                           LatestPrice = latestPrice
                                       });
            }

            return profile;
        }

        public Profile AddStockInfoForUser(string sessionId, Stock stock)
        {
//            Node<DALShare> node = shareRepository.GetShare(stock.ShareSymbol);
//            if (node == null || node.Data == null) //TODO figure out who the F is going to be null if any !!
//            {
//                //add the share node 1st
//                CurrentShareInfo currentShareInfo = shareService.GetShareInfo(stock.ShareSymbol);
//                var dalShare = new DALShare();
//                dalShare = PropertyCopy<DALShare>.CopyFrom(currentShareInfo);
//                shareRepository.CreateShare(dalShare);
//            }

            byte[] sessionIdInBytes = Encoding.Unicode.GetBytes(sessionId);
            Node<Customer> user = customerRepository.GetUserFromSession(sessionId);

            var dalStock = new DALStock
                               {
                                   CostPerShare = stock.CostPerShare,
                                   DateOfPurchase = stock.DateOfPurchase,
                                   Quantity = stock.Quantity,
                                   ShareSymbol = stock.ShareSymbol
                               };
//            dalStock = PropertyCopy<DALStock>.CopyFrom(stock);

            dalStock.DateOfPurchase = stock.DateOfPurchase;

            //now that share is created create relationship b/w user and share
            PersistenceResult<NodeReference> addedStockInfo = customerRepository.AddStockInfo(user.Data.Email,
                                                                                                      dalStock);
            if (!addedStockInfo.IsValid)
            {
                KeyValuePair<string, ModelState> error = addedStockInfo.ModelState.FirstOrDefault();
                return new Profile
                           {
                               Errors = new[]
                                            {
                                                new Error
                                                    {
                                                        Code = error.Key,
                                                        Message = error.Value.ToString()
                                                    }
                                            }
                           };
            }
            return new Profile
                       {
                           Name = user.Data.Name,
                           Stocks = new List<Stock>
                                        {
                                            stock
                                        }
                       };
        }

        #endregion
    }
}