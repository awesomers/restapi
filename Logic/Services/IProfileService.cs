using Logic.Models;

namespace Logic.Services
{
    public interface IProfileService
    {
        Profile GetProfileData(string sessionId, int pageNumber);
        Profile AddStockInfoForUser(string sessionId, Stock stock);
    }
}