using System.Collections.Generic;
using DataAccessLayer.Models;
using Logic.Models;

namespace Logic.Services
{
    public interface IUserService
    {
        LoginResponse CreateUser(RegisterCustomeData registerCustomerData);
        LoginResponse LoginUser(LoginData loginData);
        IEnumerable<Friend> SearchUser(string friend);
        ShowOffResponse ShowOffToUser(string sessionId, string email);
        IEnumerable<Friend> GetFollowers(string sessionId);
        IEnumerable<Friend> GetFollowing(string sessionId);
    }
}
