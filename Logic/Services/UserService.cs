using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using DataAccessLayer.Models;
using DataAccessLayer.Repositories;
using Logic.Models;
using Logic.Utils;
using MiscUtil.Reflection;
using Neo4jClient;

namespace Logic.Services
{
    public class UserService : IUserService
    {
        private readonly ICustomerRepository customerRepository;

        public UserService(ICustomerRepository customerRepository)
        {
            this.customerRepository = customerRepository;
        }

        #region IUserService Members

        public LoginResponse CreateUser(RegisterCustomeData registerCustomerData)
        {
            var passwordHashedData = new HashedCustomerData
                                         {
                                             Email = registerCustomerData.Email,
                                             Name = registerCustomerData.Name,
                                             Password = CommonUtils.CreateHashedPassword(registerCustomerData.Password)
                                         };

//            byte[] generatedSuccessSessionToken = CommonUtils.GenerateSuccessSessionToken();
            string generatedSuccessSessionToken = System.IO.Path.GetRandomFileName().Replace(".", string.Empty);


            PersistenceResult<NodeReference> persistenceResult =
                customerRepository
                    .CreateUser(new Customer
                                    {
                                        Email = passwordHashedData.Email,
                                        Name = passwordHashedData.Name,
                                        Password = Encoding.UTF8.GetString(passwordHashedData.Password),
                                        SessionId = generatedSuccessSessionToken
                                    });
            if (!persistenceResult.IsValid)
            {
                KeyValuePair<string, ModelState> error = persistenceResult.ModelState.FirstOrDefault();
                return new LoginResponse
                           {
                               Errors = new[]
                                            {
                                                new Error
                                                    {
                                                        Code = error.Key,
                                                        Message = error.Value.ToString()
                                                    }
                                            }
                           };
            }
            return new LoginResponse
                       {
                           SuccessSessionToken = generatedSuccessSessionToken
                       };
        }

        public LoginResponse LoginUser(LoginData loginData)
        {
            Node<Customer> user = customerRepository.GetUser(loginData.Email);
            if (user == null)
            {
                return new LoginResponse
                           {
                               Errors = new[]
                                            {
                                                new Error
                                                    {
                                                        Code = "001",
                                                        Message = "User doesnot exist. Register?"
                                                    }
                                            }
                           };
            }
            if (user.Data.Password == Encoding.UTF8.GetString(CommonUtils.CreateHashedPassword(loginData.Password)))
            {
                
                return new LoginResponse
                           {
                               SuccessSessionToken = user.Data.SessionId
                               //TODO generate a new session ID persist and return that
                           };
            }

            return new LoginResponse
                       {
                           Errors = new[]
                                        {
                                            new Error
                                                {
                                                    Code = "001",
                                                    Message = "Incorrect Password, Please try again !"
                                                }
                                        }
                       };
        }

        public IEnumerable<Friend> SearchUser(string friend)
        {
            IEnumerable<DALFriend> dalFreinds = customerRepository.SearchUser(friend);

            foreach (DALFriend dalFreind in dalFreinds)
            {
                yield return PropertyCopy<Friend>.CopyFrom(dalFreind);
            }
        }

        public IEnumerable<Friend> GetFollowers(string sessionId)
        {
            byte[] sessionIdInBytes = Encoding.Unicode.GetBytes(sessionId);

            IEnumerable<DALFriend> dalFriends = customerRepository.GetFollowers(sessionId);
            foreach (DALFriend dalFreind in dalFriends)
            {
                yield return PropertyCopy<Friend>.CopyFrom(dalFreind);
            }
        }

        public IEnumerable<Friend> GetFollowing(string sessionId)
        {
            byte[] sessionIdInBytes = Encoding.Unicode.GetBytes(sessionId);

            IEnumerable<DALFriend> dalFriends = customerRepository.GetFollowing(sessionId);
            foreach (DALFriend dalFreind in dalFriends)
            {
                yield return PropertyCopy<Friend>.CopyFrom(dalFreind);
            }
        }

        public ShowOffResponse ShowOffToUser(string sessionId, string targetFriendEmail)
        {
            byte[] sessionIdInBytes = Encoding.Unicode.GetBytes(sessionId);

            Node<Customer> customer = customerRepository.GetUserFromSession(sessionId);

            PersistenceResult<RelationshipReference> persistenceResult = customerRepository
                .ShowOff(customer.Data.Email, targetFriendEmail);
            if (persistenceResult.ModelState.IsValid)
            {
                return new ShowOffResponse();
            }
            return new ShowOffResponse
                       {
                           Errors = new[]
                                        {
                                            new Error
                                                {
                                                    Code = "005",
                                                    Message = "Could not show off to user"
                                                }
                                        }
                       };
        }

        #endregion
    }
}