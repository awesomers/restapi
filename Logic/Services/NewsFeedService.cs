using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.Models;
using DataAccessLayer.Repositories;
using Logic.Models;
using MiscUtil.Reflection;
using Neo4jClient;
using ShareService.Models;
using ShareService.Services;

namespace Logic.Services
{
    public class NewsFeedService : INewsFeedService
    {
        private readonly ICustomerRepository customerRepository;
        private readonly IShareService shareService;

        public NewsFeedService(ICustomerRepository customerRepository,
                               IShareService shareService)
        {
            this.customerRepository = customerRepository;
            this.shareService = shareService;
        }


        public IEnumerable<Friend> GetTopPerormers(string sessionId, int count)
        {
            byte[] sessionIdInBytes = Encoding.Unicode.GetBytes(sessionId);

            IEnumerable<DALFriend> dalFriends = customerRepository.GetFollowing(sessionId);
            var friends = new List<Friend>();

            foreach (DALFriend dalFriend in dalFriends)
            {
                Friend friend = PropertyCopy<Friend>.CopyFrom(dalFriend);
                IEnumerable<Node<DALStock>> stocks = customerRepository.GetStocks(dalFriend.Email);
                double totalGainFraction=0;

                foreach (Node<DALStock> stock in stocks)
                {
                    CurrentShareInfo currentShareInfo = shareService.GetShareInfo(stock.Data.ShareSymbol);
                    double gainFraction = (currentShareInfo.LatestPrice - stock.Data.CostPerShare)
                        /stock.Data.CostPerShare;
                    totalGainFraction += gainFraction*stock.Data.Quantity/stocks.Sum(node => node.Data.Quantity);
                }

                friend.Improvement = totalGainFraction;

                friends.Add(friend);
            }

            friends.Sort(Comparison);
            IEnumerable<Friend> topFriends = friends.Take(5);

            return topFriends;

        }

        private int Comparison(Friend friend, Friend friend1)
        {
            if (friend.Improvement > friend1.Improvement)
            {
                return 1;
            }
            return 0;
        }
    }

    public interface INewsFeedService
    {
        IEnumerable<Friend> GetTopPerormers(string sessionId, int count);
    }
}