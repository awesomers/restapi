﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Logic.Models;
using Logic.Services;

namespace StockRestApi.Controllers
{
    public class FriendsController : ApiController
    {
        private readonly IUserService userService;

        public FriendsController(IUserService userService)
        {
            this.userService = userService;
        }

        public HttpResponseMessage Get(string friend)
        {
            var sessionId = Request.Headers.GetValues("sessionId").FirstOrDefault();
            IEnumerable<Friend> friends = userService.SearchUser(friend);
            List<Friend> friendList = new List<Friend>();
            foreach (Friend friend1 in friends)
            {
                friendList.Add(friend1);
            }
            Friends friendsJson = new Friends()
                                   {
                                       friends = friendList
                                   };

            Friend firstOrDefault = friends.FirstOrDefault();
            HttpResponseMessage httpResponseMessage;
            if ((friends.FirstOrDefault()!= null && friends.FirstOrDefault().Errors == null) 
                || friendsJson.friends.Count==0)
            {
                httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK, friendsJson);
            }
            else
            {
                httpResponseMessage = Request.CreateResponse(HttpStatusCode.PreconditionFailed);
            }
            return httpResponseMessage;
        }

        public HttpResponseMessage Post([FromBody] string email)
        {
            var sessionId = Request.Headers.GetValues("sessionId").FirstOrDefault();
            ShowOffResponse showOffToUser = userService.ShowOffToUser(sessionId, email);

            HttpResponseMessage httpResponseMessage;
            if (showOffToUser.Errors == null)
            {
                httpResponseMessage = Request.CreateResponse(HttpStatusCode.Created, showOffToUser);
            }
            else
            {
                httpResponseMessage = Request.CreateResponse(HttpStatusCode.PreconditionFailed);
            }
            return httpResponseMessage;

        }

    }


}
