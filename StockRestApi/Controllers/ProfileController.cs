﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Logic.Models;
using Logic.Services;
using System.Linq;

namespace StockRestApi.Controllers
{
    public class ProfileController : ApiController
    {
        private readonly IProfileService profileService;

        public ProfileController(IProfileService profileService)
        {
            this.profileService = profileService;
        }

        public HttpResponseMessage Get(int page)
        {
            var sessionId = Request.Headers.GetValues("sessionId").FirstOrDefault();
            Profile profile = profileService.GetProfileData(sessionId, page);

            HttpResponseMessage httpResponseMessage;
            if (profile.Errors == null)
            {
                httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK, profile);
            }
            else
            {
                httpResponseMessage = Request.CreateResponse(HttpStatusCode.PreconditionFailed);
            }
            return httpResponseMessage;
        }
    }
}