﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Logic;
using Logic.Models;
using Logic.Services;

namespace StockRestApi.Controllers
{
    public class LoginController : ApiController
    {
        private readonly IUserService userService;

        public LoginController(IUserService userService)
        {
            this.userService = userService;
        }

        public HttpResponseMessage Post([FromBody]LoginData loginData)
        {
            HttpResponseMessage httpResponseMessage;

             LoginResponse loginResponse = userService.LoginUser(loginData);
            if (loginResponse.Errors == null)
                httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK,loginResponse);
            else
            {
                httpResponseMessage = Request.CreateResponse(HttpStatusCode.PreconditionFailed);
            }

            return httpResponseMessage;
        }

    }
}
