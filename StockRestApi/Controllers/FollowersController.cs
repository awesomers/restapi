﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Logic.Models;
using Logic.Services;

namespace StockRestApi.Controllers
{
    public class FollowersController : ApiController
    {
        private readonly IUserService userService;

        public FollowersController(IUserService userService)
        {
            this.userService = userService;
        }

        public HttpResponseMessage Get()
        {
            var sessionId = Request.Headers.GetValues("sessionId").FirstOrDefault();
            IEnumerable<Friend> friends = userService.GetFollowers(sessionId);

            HttpResponseMessage httpResponseMessage;
            if (friends.First().Errors == null)
            {
                httpResponseMessage = Request.CreateResponse(HttpStatusCode.Created, friends);
            }
            else
            {
                httpResponseMessage = Request.CreateResponse(HttpStatusCode.PreconditionFailed);
            }
            return httpResponseMessage;

        }

    }
}
