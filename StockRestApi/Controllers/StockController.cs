﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Logic.Models;
using Logic.Services;

namespace StockRestApi.Controllers
{
    public class StockController : ApiController
    {
        private readonly IProfileService profileService;

        public StockController(IProfileService profileService)
        {
            this.profileService = profileService;
        }

        public HttpResponseMessage Post([FromBody] Stock stock)
        {
            string sessionId = Request.Headers.GetValues("sessionId").SingleOrDefault();
            HttpResponseMessage httpResponseMessage;
            if (string.IsNullOrEmpty(stock.ShareSymbol))
            {
                return Request.CreateResponse(HttpStatusCode.PreconditionFailed);
            }
            Profile profile = profileService.AddStockInfoForUser(sessionId, stock);


            if (profile.Errors == null)
            {
                httpResponseMessage = Request.CreateResponse(HttpStatusCode.Created, profile);
            }
            else
            {
                httpResponseMessage = Request.CreateResponse(HttpStatusCode.PreconditionFailed);
            }
            return httpResponseMessage;
        }


    }
}
