﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Logic.Models;
using Logic.Services;

namespace StockRestApi.Controllers
{
    public class NewsfeedController : ApiController
    {
        private readonly INewsFeedService newsfeedService;

        public NewsfeedController(INewsFeedService newsfeedService)
        {
            this.newsfeedService = newsfeedService;
        }

        public HttpResponseMessage Get()
        {
            var sessionId = Request.Headers.GetValues("sessionId").FirstOrDefault();
            IEnumerable<Friend> topPerormersOfTheDay = newsfeedService.GetTopPerormers(sessionId,5);

            HttpResponseMessage httpResponseMessage;
            if (topPerormersOfTheDay.FirstOrDefault().Errors== null)
            {
                httpResponseMessage = Request.CreateResponse(HttpStatusCode.Created, topPerormersOfTheDay);
            }
            else
            {
                httpResponseMessage = Request.CreateResponse(HttpStatusCode.PreconditionFailed);
            }
            return httpResponseMessage;
        }
    }
}
