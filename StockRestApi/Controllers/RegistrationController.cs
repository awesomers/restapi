﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Logic;
using Logic.Models;
using Logic.Services;

namespace StockRestApi.Controllers
{
    public class RegistrationController : ApiController
    {
        private readonly IUserService userService;

        public RegistrationController(IUserService userService)
        {
            this.userService = userService;
        }

        // GET api/values
        //remove this later
        public IEnumerable<string> Get()
        {
            return new[] {"value1", "value2"};
        }

        // GET api/values/5
        //remove this later
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        //POST to controller/registration/
        public HttpResponseMessage Post([FromBody] RegisterCustomeData customerData)
        {
            HttpResponseMessage httpResponseMessage;

            LoginResponse registrationResponse = userService.CreateUser(customerData);
            if (registrationResponse.Errors == null)
            {
                httpResponseMessage = Request.CreateResponse(HttpStatusCode.Created, registrationResponse);
            }
            else
            {
                httpResponseMessage = Request.CreateResponse(HttpStatusCode.PreconditionFailed);
            }

            return httpResponseMessage;
        }

        // PUT api/values/5
        //remove this later
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        //remove this later ?? i think, if we're not using this to unregister
        public void Delete(int id)
        {
        }
    }
}