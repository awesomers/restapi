﻿using System;
using System.Configuration;
using Neo4jClient;

namespace StockRestApi.Factory
{
    public class GraphClientFactory
    {
        public static GraphClient CreateNewGraphClient()
        {
            string dbData = ConfigurationManager.AppSettings["GraphDatabaseUrl"];
            dbData= (string.IsNullOrEmpty(dbData)) ? "http://localhost:7474/db/data" : dbData;
            string dbUsername = ConfigurationManager.AppSettings["GraphDatabaseUsername"];
            dbUsername = (string.IsNullOrEmpty(dbUsername)) ? "" : dbUsername;
            string dbPassword = ConfigurationManager.AppSettings["GraphDatabasePassword"];
            dbPassword = (string.IsNullOrEmpty(dbPassword)) ? "" : dbPassword;

            var graphClient = new GraphClient(new Uri(dbData));
            graphClient.Connect(dbUsername,dbPassword);
            return graphClient;
        }
    }
}