﻿using System;
using Castle.Core;
using Castle.Facilities.FactorySupport;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using DataAccessLayer.Repositories;
using Logic;
using Logic.Services;
using Neo4jClient;
using ShareService.Services;
using StockRestApi.Controllers;
using StockRestApi.Factory;

namespace StockRestApi.Installers
{
    public class ControllersInstaller : IWindsorInstaller
    {
        #region IWindsorInstaller Members

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.AddFacility<FactorySupportFacility>();
            container.Register(
                Component.For<IGraphClient>().LifeStyle.Is(LifestyleType.Singleton).UsingFactoryMethod(
                GraphClientFactory.CreateNewGraphClient
                ),
                Component.For<ICustomerRepository>().ImplementedBy<CustomerRepository>().LifeStyle.Is(
                    LifestyleType.PerWebRequest),
                Component.For<IShareRepository>().ImplementedBy<ShareRepository>().LifeStyle.Is(
                    LifestyleType.PerWebRequest),
                Component.For<IShareService>().ImplementedBy<GoogleShareService>().LifeStyle.Is(LifestyleType.PerWebRequest),
                Component.For<IUserService>().ImplementedBy<UserService>().LifeStyle.Is(LifestyleType.PerWebRequest),
                Component.For<IProfileService>().ImplementedBy<ProfileService>().LifeStyle.Is(LifestyleType.PerWebRequest),
                Component.For<INewsFeedService>().ImplementedBy<NewsFeedService>().LifeStyle.Is(LifestyleType.PerWebRequest),
                Component.For<HomeController>().ImplementedBy<HomeController>().LifeStyle.Is(
                    LifestyleType.PerWebRequest),
                Component.For<ProfileController>().ImplementedBy<ProfileController>().LifeStyle.Is(
                    LifestyleType.PerWebRequest),
                Component.For<RegistrationController>().ImplementedBy<RegistrationController>().LifeStyle.Is(
                    LifestyleType.PerWebRequest),
                Component.For<LoginController>().ImplementedBy<LoginController>().LifeStyle.Is(
                    LifestyleType.PerWebRequest),
                Component.For<StockController>().ImplementedBy<StockController>().LifeStyle.Is(
                    LifestyleType.PerWebRequest),
                Component.For<FriendsController>().ImplementedBy<FriendsController>().LifeStyle.Is(
                    LifestyleType.PerWebRequest),
                Component.For<FollowersController>().ImplementedBy<FollowersController>().LifeStyle.Is(
                    LifestyleType.PerWebRequest),
                Component.For<FollowingController>().ImplementedBy<FollowingController>().LifeStyle.Is(
                    LifestyleType.PerWebRequest),
                Component.For<NewsfeedController>().ImplementedBy<NewsfeedController>().LifeStyle.Is(
                    LifestyleType.PerWebRequest)
                );
        }



        #endregion
    }
}